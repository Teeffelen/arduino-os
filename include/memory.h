/*
 * Memory Manager
 */

#pragma once

#include <Arduino.h>
#include "stack.h"

#define TABLE_SIZE  25
#define MEMORY_SIZE 256

namespace memory
{
    // Memory variable structure
    struct variable {
        char name;
        unsigned int processID;
        char type;
        byte size;
        unsigned int address;
    };

    // Namespace variables
    extern byte noOfVars;
    extern byte memory[MEMORY_SIZE];
    extern variable varTable[TABLE_SIZE];

    // Memory functions
    int getVarIndex(byte name, int processID);
    int setVar(byte name, int processID, stack::stack* stack);
    int getVar(byte name, int processID, stack::stack* stack);
    void clearVars(int processID);
    void deleteVar(byte index);
    int getStartPos(int size);
};
