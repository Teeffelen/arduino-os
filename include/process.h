/*
 * Process Manager
 */

#pragma once

#include <Arduino.h>
#include "storage.h"
#include "stack.h"

#define PROCESS_TABLE_SIZE 10
#define MAX_FILENAME 12
#define RUN_PROCESS_TIME 50 // Time in millis for each program cycle

namespace process
{
    // Process structure
    struct process {
        char name[MAX_FILENAME];
        byte processID;
        char state; // running 'r', paused 'p', terminated 't'
        int proCnt; // Program Counter
        int filePtr; // File pointer
        stack::stack stack; // Process stack
        int loopAddress; // Address used to store LOOP address
    };

    // Namespace variables
    extern process proTable[PROCESS_TABLE_SIZE];
    extern byte noOfProcess;

    // Process functions
    void runPrograms();
    int processStart(char* fileName);
    void processKill(int index);
    void processList();
    int processExists(int index);
    bool setState(int index, char state);
    void printProcess(int index);
    bool execute(int index);
};
