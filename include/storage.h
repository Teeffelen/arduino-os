/*
 * File Allocation Table
 */

#pragma once

#include <Arduino.h>
#include <EEPROM.h>

#define FILE_CNT_POS    0   // noOfFiles byte
#define FAT_START       1   // FAT start byte
#define FAT_SIZE        10  // Files FAT can store
#define MAX_FILENAME    12  // Max filename length (zero terminated = -1)

namespace storage
{
    // Eeprom file structure
    typedef struct {
        char name[MAX_FILENAME];
        int startPos;
        int size;
    } eepromFile;

    // Storage variables
    extern EERef noOfFiles;

    // Storage functions
    void initFAT();
    int addFile(char* name, int size, byte* data);
    bool readFile(char* name);
    bool deleteFile(char* name);
    void listFiles();
    int freeMemSpace();

    // Helper functions
    int nameToFATEntry(char* name);
    int firstEmptyFile();
    int getStartPos(int index);
    int getFileSize(int index);
    int getFreeStartPos(int size);
    int getNextFileStartPos(int index);

    // Read/write FAT entries
    void writeFATEntry(byte entryPos, eepromFile file);
    eepromFile readFATEntry(byte entryPos);

    // Store data to EEPROM
    void writeData(int startPos, int size, byte* data);
};
