/*
 * Stack Manager
 */

#pragma once

#include <Arduino.h>

#define STACK_SIZE 32

namespace stack
{
    // Stack structure
    struct stack
    {
        byte stack[STACK_SIZE];
        uint_least8_t sp;
    };

    // Bytes
    void pushByte(stack* stack, byte b);
    byte popByte(stack* stack, bool peek = false);

    // Integers
    void pushInt(stack* stack, int b);
    int popInt(stack* stack);

    // Floats
    void pushFloat(stack* stack, float b);
    float popFloat(stack* stack);

    // Chars
    void pushChar(stack* stack, char c);
    char popChar(stack* stack);

    // Strings
    void pushString(stack* stack, char* str, uint_least8_t length);
    char* popString(stack* stack);

    // Char, Int or Float
    float popVal(stack* stack, bool peek = false);
};
