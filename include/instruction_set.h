#define CHAR 1 // 1-byte // put on stack (value, type)
#define INT 2 // 2-byte // put on stack (value, type)
#define STRING 3 // n-byte (zero-terminated) // put on stack (string, length (byte), type)
#define FLOAT 4 // 4-byte // put on stack (value, type)
#define SET 5 // name x // put in memory (name is 1 byte, not from stack)
#define GET 6 // name // retrieve from memory and put on stack
#define INCREMENT 7 // x
#define DECREMENT 8 // x
#define PLUS 9 // x y
#define MINUS 10 // x y
#define TIMES 11 // x y
#define DIVIDEDBY 12 // x y
#define MODULUS 13 // x y
#define UNARYMINUS 14 // x
#define EQUALS 15 // x y
#define NOTEQUALS 16 // x y
#define LESSTHAN 17 // x y
#define LESSTHANOREQUALS 18 // x y
#define GREATERTHAN 19 // x y
#define GREATERTHANOREQUALS 20 // x y
#define LOGICALAND 21 // x y
#define LOGICALOR 22 // x y
#define LOGICALXOR 23 // x y
#define LOGICALNOT 24 // x
#define BITWISEAND 25 // x y
#define BITWISEOR 26 // x y
#define BITWISEXOR 27 // x y
#define BITWISENOT 28 // x
#define TOCHAR 29
#define TOINT 30 // x
#define TOFLOAT 31 // x
#define ROUND 32 // x
#define FLOOR 33 // x
#define CEIL 34 // x
#define MIN 35 // x y
#define MAX 36 // x y
#define ABS 37 // x
#define CONSTRAIN 38 // x a b
#define MAP 39 // value fromLow fromHigh toLow toHigh
#define POW 40 // base exponent
#define SQ 41 // x
#define SQRT 42 // x
#define DELAY 43 // millis
#define DELAYUNTIL 44 // millis
#define MILLIS 45
#define PINMODE 46 // x y
#define ANALOGREAD 47 // pin
#define ANALOGWRITE 48 // pin val
#define DIGITALREAD 49 // pin
#define DIGITALWRITE 50 // pin val
#define PRINT 51 // data // output to console
#define PRINTLN 52 // data // output to console
#define OPEN 53 // name length // name is zero-terminated string
#define CLOSE 54
#define WRITE 55 // data // output to file
#define READINT 56 // read from file
#define READCHAR 57 // read from file
#define READFLOAT 58 // read from file
#define READSTRING 59 // read from file
#define IF 128 // lengthOfTrueCode // increments lengthOfTrueCode if 0 on stack.
#define ELSE 129 // lengthOfFalseCode // increments lengthOfFalseCode if 0 not on stack.
#define ENDIF 130 // pops 1 value from stack
#define WHILE 131 // lengthOfConditionCode lengthOfRepeatedCode // pops 1 value. jumps lengthOfRepeatedCode + 1 ahead if value is 0. if not, pushes lengthOfConditionCode + lengthOfRepeatedCode + 4 on stack
#define ENDWHILE 132 // pops 1 value and jumps back to while
#define LOOP 133 // dictates setup() ended and loop() starts
#define ENDLOOP 134
#define STOP 135
#define FORK 136 // name // starts new process, returns process ID
#define WAITUNTILDONE 137 // processID
