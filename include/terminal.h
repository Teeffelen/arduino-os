/*
 * Terminal
 */

#pragma once

#include <Arduino.h>

#define MAX_COMMAND_NAME_CHAR   12
#define MAX_ARGUMENT_CHAR       MAX_FILENAME
#define MAX_COMMAND_ARGS        2

namespace terminal
{
    // Variables
    extern char* curCommand;
    extern bool commandTypeReady;
    extern char** curArgs;
    extern int curArgIter;

    // Functions
    void init();
    void runTerminal();
    bool readSerialInput(); // Read from Serial Buffer
    bool writeCommand(char inputChar); // Store command
    bool writeArg(char inputChar); // Store arguments
    int checkCommand(char* inputCommand, char** args); // Validate command
    void printInvalidCommand(int errorCode);
    void printCommand();
    void runCommand(byte location, char** args);
    char* chrcat(char* appendTo, char what);

    // Command functions
    void help(char** args);         // Show all commands
    void createFAT(char** args);    // Init a empty FAT
    void store(char** args);        // Store a file in FAT
    void retrieve(char** args);     // Retrieve a file from FAT
    void erase(char** args);        // Erase a file from FAT
    void files(char** args);        // Show all files
    void freespace(char** args);    // Show available file space
    void run(char** args);          // Run a program
    void list(char** args);         // List all processes
    void suspend(char** args);      // Suspend a process
    void resume(char** args);       // Resume a process
    void kill(char** args);         // Kill a process

    // Command structure
    struct command {
        char name[MAX_COMMAND_NAME_CHAR];       // Command name
        void (*commandFunction)(char** args);   // Command function
        byte amountArgs;                        // Required arguments
    };

    // List of available commands (structs)
    const static command commandList[] =
    {// Command         function ptr            arguments required
        {"help",        &terminal::help,        0},
        {"initFAT",     &terminal::createFAT,   0},
        {"store",       &terminal::store,       2}, // filename data
        {"retrieve",    &terminal::retrieve,    1}, // filename
        {"erase",       &terminal::erase,       1}, // filename
        {"files",       &terminal::files,       0},
        {"freespace",   &terminal::freespace,   0},
        {"run",         &terminal::run,         1}, // filename
        {"list",        &terminal::list,        0},
        {"suspend",     &terminal::suspend,     1}, // processID
        {"resume",      &terminal::resume,      1}, // processID
        {"kill",        &terminal::kill,        1}, // processID
    };
};
