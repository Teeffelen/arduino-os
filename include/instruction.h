/*
 * Instruction Manager
 */

#pragma once

#include <Arduino.h>
#include "process.h"

namespace instruction
{
    // Values
    void valueToStack(process::process* process, byte type);

    // Unary operations
    void unaryOperation(process::process* process, byte operation);

    // Binary operations
    void binaryOperation(process::process* process, byte operation);

    // Arduino operations
    void arduinoOperation(process::process* process, byte operation);

    // Time operations
    void timeOperation(process::process* process, byte operation);

    // Branch operations
    void branchOperation(process::process* process, byte operation);

    // Printing
    void print(process::process* process, bool newline=false);
};
