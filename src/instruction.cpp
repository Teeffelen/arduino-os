/*
 * Instruction Manager
 */

#include "instruction.h"
#include "instruction_set.h"
#include "stack.h"
#include <EEPROM.h>

//==============================================================================
// Reads value from storage and pushes it onto the stack
// Credit: Thomas Graafland
void instruction::valueToStack(process::process* process, byte type)
{
    if (type == STRING) // Seperate strings to determine size
    {
        uint_least8_t size = 0;
        char nextChar = 0;
        do
        {
            nextChar = EEPROM[process->proCnt++];
            size++; // Increase size
            stack::pushByte(&(process->stack), nextChar); // Push chars on stack
        }
        while (nextChar != 0);
        stack::pushByte(&(process->stack), size); // Push size on stack
    }
    else // CHAR, INT or FLOAT
    {
        for (uint_least8_t i = 0; i < type; i++) // Push bytes on stack
            stack::pushByte(&(process->stack), EEPROM[process->proCnt++]);
    }
    stack::pushByte(&(process->stack), type); // Push datatype on stack
}

//==============================================================================
// Unary operations
// Pops a value from stack and pushes resulting value back onto stack
void instruction::unaryOperation(process::process* process, byte operation)
{
    // Peek type so we can push it correctly after a operation
    byte type = stack::popByte(&(process->stack), true);

    // Get the value as float from stack
    float value = stack::popVal(&(process->stack));

    switch (operation) // Perform operation on value as float
    {
        case (INCREMENT):
            value += 1.0f;
            break;
        case (DECREMENT):
            value -= 1.0f;
            break;
        case (UNARYMINUS):
            value *= -1.0f;
            break;
        case (ABS):
            value = abs(value);
            break;
        case (SQ):
            value = value * value;
            break;
        case (SQRT):
            value = sqrt(value);
            break;
        case (ANALOGREAD):
            value = analogRead(value);
            type = INT; // Push as INT
            break;
        case (DIGITALREAD):
            value = digitalRead(value);
            type = CHAR; // Push as CHAR
            break;
        case (LOGICALNOT):
            value = !value;
            type = CHAR; // Push as CHAR
            break;
        case (BITWISENOT):
            value = ~(int) value; // C++ will cast the integer back to a float
            break;
        case (TOCHAR):
            value = round(value);
            type = CHAR; // Push as CHAR
            break;
        case (TOINT):
        case (ROUND): // Does the same as TOINT
            value = round(value);
            type = INT; // Push as INT
            break;
        case (TOFLOAT):
            type = FLOAT; // Push as FLOAT
            break;
        case (FLOOR):
            value = floor(value);
            type = INT; // Push as INT
            break;
        case (CEIL):
            value = ceil(value);
            type = INT; // Push as INT
            break;
        default:
            Serial.print(F("unknown unary operation: "));
            Serial.println(operation, DEC);
    }

    // Push value back on the stack by type
    if (type == CHAR)
        stack::pushChar(&(process->stack), (char) value);
    else if (type == INT)
        stack::pushInt(&(process->stack), (int) value);
    else if (type == FLOAT)
        stack::pushFloat(&(process->stack), value);
}

//==============================================================================
// Binary operations
// Pops a X and Y from stack and pushes resulting Z back onto stack
void instruction::binaryOperation(process::process* process, byte operation)
{
    // Use X and Y as input values
    // Pop Y first as it is at the top of the stack
    byte ytype = stack::popByte(&(process->stack), true); // Peek type
    float y = stack::popVal(&(process->stack)); // Pop value as float
    byte xtype = stack::popByte(&(process->stack), true); // Peek type
    float x = stack::popVal(&(process->stack)); // Pop value as float

    // Use X as output value, type is determined by the largest type of X or Y
    xtype = (xtype > ytype ? xtype : ytype); // Get largest type

    switch (operation) // Perform operation on values as floats
    {
        case (PLUS):
            x = x + y;
            break;
        case (MINUS):
            x = x - y;
            break;
        case (TIMES):
            x = x * y;
            break;
        case (DIVIDEDBY):
            x = x / y;
            break;
        case (MODULUS):
            x = (int) x % (int) y;
            break;
        case (EQUALS):
            x = (x == y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (NOTEQUALS):
            x = (x != y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (LESSTHAN):
            x = (x < y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (LESSTHANOREQUALS):
            x = (x <= y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (GREATERTHAN):
            x = (x > y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (GREATERTHANOREQUALS):
            x = (x >= y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (MIN):
            x = (x < y ? x : y);
            break;
        case (MAX):
            x = (x > y ? x : y);
            break;
        case (POW):
            x = pow(x, y);
            break;
        case (LOGICALAND):
            x = (x && y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (LOGICALOR):
            x = (x || y ? 1 : 0);
            xtype = CHAR; // Push as CHAR
            break;
        case (LOGICALXOR):
            x = (!x != !y ? 1 : 0); // Invert X and Y to create boolean values
            xtype = CHAR; // Push as CHAR
            break;
        case (BITWISEAND): // INT, CHAR only
            x = (int) x & (int) y;
            break;
        case (BITWISEOR): // INT, CHAR only
            x = (int) x | (int) y;
            break;
        case (BITWISEXOR): // INT, CHAR only
            x = (int) x ^ (int) y;
            break;
        default:
            Serial.print(F("unknown binary operation: "));
            Serial.println(operation, DEC);
    }

    // Push output value (float z) back on the stack by ztype
    if (xtype == CHAR)
        stack::pushChar(&(process->stack), (char) x);
    else if (xtype == INT)
        stack::pushInt(&(process->stack), (int) x);
    else if (xtype == FLOAT)
        stack::pushFloat(&(process->stack), x);
}

//==============================================================================
// Arduino operations
void instruction::arduinoOperation(process::process* process, byte operation)
{
    switch (operation) // Check operation
    {
        case (CONSTRAIN):
        {
            // Pop in reverse order
            int max = (int) stack::popVal(&(process->stack)); // Maximum
            int min = (int) stack::popVal(&(process->stack)); // Minimum
            int x = (int) stack::popVal(&(process->stack)); // Constrain value
            x = min(max(x, min), max); // Constrain x
            stack::pushInt(&(process->stack), x); // Push result
            break;
        }
        case (MAP):
        {
            // Pop in reverse order
            int out_max = (int) stack::popVal(&(process->stack));
            int out_min = (int) stack::popVal(&(process->stack));
            int in_max = (int) stack::popVal(&(process->stack));
            int in_min = (int) stack::popVal(&(process->stack));
            int x = (int) stack::popVal(&(process->stack)); // Value to map
            x = map(x, in_min, in_max, out_min, out_max); // Map x
            stack::pushInt(&(process->stack), x); // Push result
            break;
        }
        case (PINMODE):
        case (DIGITALWRITE):
        case (ANALOGWRITE):
        {
            // Pop in reverse order
            int value = (int) stack::popVal(&(process->stack));
            int pin = (int) stack::popVal(&(process->stack));
            if (operation == PINMODE)
                pinMode(pin, value); // Set pinmode
            else if (operation == ANALOGWRITE)
                analogWrite(pin, value); // Set analog pin value
            else if (operation == DIGITALWRITE)
                digitalWrite(pin, value); // Set digital pin value
            break;
        }
        default:
            Serial.print(F("unknown arduino operation: "));
            Serial.println(operation, DEC);
    }
}

//==============================================================================
// Time operations
void instruction::timeOperation(process::process* process, byte operation)
{
    switch (operation) // Check operation
    {
        case (DELAY):
            delay((int) stack::popVal(&(process->stack))); // Delay
            break;
        case (DELAYUNTIL):
        {
            // Get delay from stack and cast to an integer
            int x = (int) stack::popVal(&(process->stack));
            if (x > (int) millis()) // Check if delay has passed
            {
                stack::pushInt(&(process->stack), x); // Push x back on stack
                process->proCnt -= 1; // Delay not done, so lower PC
            }
            break;
        }
        case (MILLIS):
            stack::pushInt(&(process->stack), (int) millis());
            break;
        default:
            Serial.print(F("unknown time operation: "));
            Serial.println(operation, DEC);
    }
}

//==============================================================================
// Time operations
void instruction::branchOperation(process::process* process, byte operation)
{
    switch (operation)
    {
        case (IF):
        {
            // Peek value and cast to char (single byte)
            char x = (char) stack::popVal(&(process->stack), true);
            if (x == 0)
                // Jump N amount of bytes (N follows the IF statement)
                process->proCnt += 1 + EEPROM[process->proCnt];
            break;
        }
        case (ELSE):
        {
            // Peek value and cast to char (single byte)
            char x = (char) stack::popVal(&(process->stack), true);
            if (x != 0)
                // Jump N amount of bytes (N follows the ELSE statement)
                // +1 to skip N itself
                process->proCnt += 1 + EEPROM[process->proCnt];
            break;
        }
        case (ENDIF):
            stack::popVal(&(process->stack)); // Pop x from stack
            break;
        case (WHILE):
        {
            // Peek value and cast to char (single byte)
            char x = (char) stack::popVal(&(process->stack), true);
            if (x == 0) // Skip WHILE
            {
                // Jump N amount of bytes (N is the second ELSE argument, so +1)
                // +2 to skip both WHILE arguments
                process->proCnt += 2 + EEPROM[process->proCnt + 1];
            }
            else // Run WHILE
            {
                // Push bytes of WHILE body and condition
                stack::pushByte(&(process->stack),
                                4 + (EEPROM[process->proCnt] +
                                EEPROM[process->proCnt + 1]));
                process->proCnt += 2; // +2 to skip both WHILE arguments
            }
            break;
        }
        case (ENDWHILE):
        {
            byte steps = stack::popByte(&(process->stack));
            process->proCnt -= steps; // Go back <steps> number of bytes
            break;
        }
        case (LOOP):
            process->loopAddress = process->proCnt; // Save loop address
            break;
        case (ENDLOOP):
            process->proCnt = process->loopAddress; // Set PC to loop address
            break;
        default:
            Serial.print(F("unknown branch operation: "));
            Serial.println(operation, DEC);
    }
}

//==============================================================================
// Print
// Credit: Thomas Graafland
void instruction::print(process::process* process, bool newline=false)
{
    byte type = stack::popByte(&(process->stack), true); // Peek datatype

    switch(type) // Print by popping the datatype
    {
        case (CHAR):
            Serial.print(stack::popChar(&(process->stack)));
            break;
        case (STRING):
            Serial.print(stack::popString(&(process->stack)));
            break;
        case (INT):
            Serial.print(stack::popInt(&(process->stack)), DEC);
            break;
        case (FLOAT):
            Serial.print(stack::popFloat(&(process->stack)), 3); // 3 decimals
    }

    if (newline) Serial.println(); // Add a newline if PRINTLN was used
}
