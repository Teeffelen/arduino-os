/*
 * File Allocation Table
 */

#include "storage.h"

EERef storage::noOfFiles = EEPROM[FILE_CNT_POS];

//==============================================================================
// Initialize File Allocation Table
void storage::initFAT()
{
    noOfFiles = 0; // Reset number of files

    Serial.print(F("last FAT byte: "));
    Serial.println(sizeof(noOfFiles) + (sizeof(eepromFile) * FAT_SIZE));

    // Write empty FAT entries
    for (byte i = 0; i < FAT_SIZE; i++)
    {
        eepromFile emptyFile = (eepromFile){"",0,0};
        writeFATEntry(i, emptyFile);
    }
}

//==============================================================================
// Write and read from FAT
void storage::writeFATEntry(byte entryPos, eepromFile file)
{
    // Write file to EEPROM
    EEPROM.put(FAT_START + entryPos * sizeof(eepromFile), file);
}
storage::eepromFile storage::readFATEntry(byte entryPos)
{
    eepromFile file;
    // Read file from EEPROM
    EEPROM.get(FAT_START + entryPos * sizeof(eepromFile), file);
    return file;
}

//==============================================================================
// Add a file to FAT & EEPROM
// Credit: Max van Meijeren
// returns 1 if file is stored
// returns -1 if filename to long
// returns -2 if not enough space in FAT or storage
// returns -3 if filename exists
int storage::addFile(char* name, int size, byte* data)
{
    if (strlen(name) > MAX_FILENAME) // Check filename
        return -1;
    if (noOfFiles == FAT_SIZE) // Check FAT size
        return -2;
    if (nameToFATEntry(name) != -1) // Check if name does not exist
        return -3;

    // Files might be scattered and leave bits of freespace
    //  getFreeStartPos() checks if this is the case
    int startPos = getFreeStartPos(size); // Only get startPos once
    if (startPos == -1)
        return -2; // No free space

    // Store the file
    eepromFile file = (eepromFile){"", startPos, size};
    strcpy(file.name, name); // Copy name
    writeFATEntry(firstEmptyFile(), file); // Store file in FAT
    writeData(file.startPos, size, data); // store data in EERPROM

    noOfFiles += 1; // We added a file, hooray!
    return 1;
}

//==============================================================================
// Read a file from FAT & EEPROM
// Returns true if succesfull
// Returns false if file could not be found
bool storage::readFile(char* name)
{
    int index = nameToFATEntry(name);
    if (index == -1)
        return false; // File does not exist

    int startPos = storage::getStartPos(index); // Get start address
    int size = getFileSize(index); // Get file size
    for (int i = 0; i < size; i++)
    {
        Serial.print(EEPROM[startPos+i], DEC); // Print each byte from EEPROM
        Serial.print(F(" ")); // Space between bytes
        if (i > 0 && i != size-1 && i % 10 == 0)
            Serial.println(); // Print only 10 bytes per line
    }
    Serial.println(); // Don't forget to end the line
    return true;
}

//==============================================================================
// Delete a file from FAT & EEPROM
bool storage::deleteFile(char* name)
{
    int entry = nameToFATEntry(name); // Get entry number from filename
    if (entry >= 0)
    {
        eepromFile file = readFATEntry(entry);
        file.name[0] = '\0'; // Clear filename
        file.startPos = 0;
        file.size = 0;
        writeFATEntry(entry, file); // Write empty FAT entry
        noOfFiles -= 1; // We deleted a file, hooray!
        return true;
    }
    else
        return false; // File not found
}

//==============================================================================
// Write data to EEPROM
// Credit: Max van Meijeren
void storage::writeData(int startPos, int size, byte* data)
{
    int nextAddress = 0;
    for (int i = 0; i < size; i++) // Store all bytes to disk
    {
        EEPROM.write(startPos + nextAddress, data[i]); // Store byte on EERPOM
        nextAddress += sizeof(data[i]); // Skip to next address
    }
}

//==============================================================================
// List files from FAT
void storage::listFiles()
{
    eepromFile file;
    for (byte i = 0; i < FAT_SIZE; i++) // Go through all files in FAT
    {
        file = readFATEntry(i);
        if (file.size > 0) // Only print files that contain data
        {
            Serial.print(F("file: "));
            Serial.print(i); // File index from FAT
            Serial.print(F("\t- "));
            Serial.print(file.name); // Filename
            Serial.print(F("\t\t("));
            Serial.print(file.startPos); // Start byte on disk
            Serial.print(F("/"));
            Serial.print(file.startPos + file.size - 1); // End byte on disk
            Serial.print(F(" - "));
            Serial.print(file.size); // Filesize
            Serial.println(F(" bytes)"));
        }
    }
}

//==============================================================================
// Get FAT number from file name
int storage::nameToFATEntry(char* name)
{
    for (byte i = 0; i < FAT_SIZE; i++) // Go through all files in FAT
    {
        eepromFile file = readFATEntry(i);
        if (strcmp(file.name, name) == 0) return i; // Return entry ID
    }
    return -1; // Name does not exist
}

//==============================================================================
// Get first empty file in FAT
// Returns <file position> if a empty file is found
// Returns -1 if no empty file is found
int storage::firstEmptyFile()
{
    for (byte i = 0; i < FAT_SIZE; i++) // Go through all files in FAT
    {
        eepromFile file = readFATEntry(i);
        if (file.size == 0) return i; // Return first free file
    }
    return -1;
}

//==============================================================================
// Return amount of free bytes
int storage::freeMemSpace()
{
    int freeMemorySpace = EEPROM.length(); // Get EEPROM size

    freeMemorySpace -= sizeof(noOfFiles); // Substract noOfFiles
    freeMemorySpace -= (sizeof(eepromFile) * FAT_SIZE); // Substract FAT size

    for (byte i = 0; i < FAT_SIZE; i++) // Go trough all files in FAT
    {
        eepromFile file = readFATEntry(i);
        if (file.size != 0) freeMemorySpace -= file.size;
    }

    return freeMemorySpace;
}

//==============================================================================
// Get startPos from file index
int storage::getStartPos(int index)
{
    eepromFile file = readFATEntry(index);
    return file.startPos; // Return the start postion on EEPROM
}

//==============================================================================
// Get file size from file index
int storage::getFileSize(int index)
{
    eepromFile file = readFATEntry(index);
    return file.size; // Return file size
}

//==============================================================================
// Get first available byte to store <size> of data
// Returns first byte  if space is found
// Returns -1          if no space is available
int storage::getFreeStartPos(int size)
{
    // Get first free byte
    // noOfFiles + FAT size + padding byte
    int firstFreeByte = sizeof(noOfFiles) + (sizeof(eepromFile) * FAT_SIZE) + 1;

    if (noOfFiles == 0)
        return firstFreeByte;  // Write first file byte after FAT

    // Check space between files or space untill end of EEPROM
    bool firstFile = true;
    for (byte i = 0; i < FAT_SIZE; i++) // Already checked 0, so start at 1
    {
        eepromFile file = readFATEntry(i); // Get file
        if (file.size > 0) // Check if file has data
        {
            // Check the first file in FAT
            if (firstFile && (file.startPos - firstFreeByte > size))
                return firstFreeByte; // Write data after FAT
            firstFile = false;

            // Check if there is space between current file and next
            //  file (or EERPOM end if no more files exist)
            if (size < getNextFileStartPos(i) - (file.startPos + file.size))
                return (file.startPos + file.size); // Write after file
        }
    }

    return -1; // Found no space large enough
}

//==============================================================================
// Returns startPos of next file if next file is found
// Returns end of EEPROM if no next file is found
int storage::getNextFileStartPos(int index)
{
    for (byte n = index + 1; n < FAT_SIZE; n++) // Go through all files in FAT
    {
        eepromFile file = readFATEntry(n);
        if (file.size != 0) return file.startPos;
    }
    return (EEPROM.length() + 1); // Add one so we can use last byte of EEPROM
}
