/*
 * Terminal
 */

#include "terminal.h"
#include "process.h"
#include "storage.h"

char* terminal::curCommand = new char[MAX_COMMAND_NAME_CHAR];
char** terminal::curArgs = new char*[MAX_COMMAND_ARGS];
int terminal::curArgIter = 0;
bool terminal::commandTypeReady = false;

//==============================================================================
// Initialize the terminal
void terminal::init()
{
    curCommand[0] = '\0'; // Empty current command array
    for (byte i = 0; i < MAX_COMMAND_ARGS; i++)
    {
        curArgs[i] = new char[MAX_ARGUMENT_CHAR];
        curArgs[i][0] = '\0'; // Emtpy current argument array
    }
    curArgIter = 0; // Reset argument counter
    commandTypeReady = false; // Ready to receive new command
}

//==============================================================================
// Run terminal and handle input if command/args are given
// Credit: Max van Meijeren
void terminal::runTerminal()
{
    if (!readSerialInput()) // Read serial char by char each runTerminal()
        return; // No command ready yet

    printCommand(); // Print Command and arguments

    // Validate command
    int commandLocation = checkCommand(curCommand, curArgs);
    if (commandLocation < 0)
        printInvalidCommand(commandLocation); // Handle invalid command
    else
        runCommand(commandLocation, curArgs); // Handle valid command

    // Reset terminal buffers for next command
    curCommand[0] = '\0'; // Empty current command array
    for (byte i = 0; i < MAX_COMMAND_ARGS; i++)
        curArgs[i][0] = '\0'; // Emtpy current argument array
    curArgIter = 0; // Reset argument counter
    commandTypeReady = false; // Ready to receive new command
}

//==============================================================================
// Read from serial
// Credit: Max van Meijeren
// Returns false if there is no command are stored (yet)
// Returns true if a command and arguments are stored
bool terminal::readSerialInput()
{
    if (!Serial.available())
        return false; // False if no input is given

    bool done = false; // Not done reading serial, so false
    char input; // Input char

    // Start reading command
    for (byte i = 0; i < Serial.available(); i++)
    {
        input = Serial.read(); // Read input
        if (commandTypeReady == true) // Start reading arguments after command
            curArgIter += writeArg(input); // add 1 when argument is finished
        if (commandTypeReady == false) // Read command
            commandTypeReady = writeCommand(input);
        if (input == '\n')
            done = true; // End of input (serial.available)
    }
    return done; // Return if reading command/arguments is done
}

//==============================================================================
// Store command
// Credit: Max van Meijeren
// Returns true if command is finished
// Returns false if command is not finished yet
bool terminal::writeCommand(char inputChar)
{
    if (inputChar == ' ' || inputChar == '\n')
        return true; // Command finished

    curCommand = chrcat(curCommand, inputChar); // Add char to command
    return false; // Command not finished yet
}

// Store arguments
// Credit: Max van Meijeren
// Returns true if arguments are finished
// Returns false if arguments are not finished yet
bool terminal::writeArg(char inputChar)
{
    if (inputChar == ' ' || inputChar == '\n')
        return true; // Argument finished

    // Add char to argument
    curArgs[curArgIter] = chrcat(curArgs[curArgIter], inputChar);
    return false; // Argument not finished yet
}

//==============================================================================
// Check for valid command and arguments
// Returns -1 if command not found
// Returns -2 if to few args are given
// Returns >=0 if command is valid
// Credit: Max van Meijeren
int terminal::checkCommand(char* inputCommand, char** args)
{
    int index = -1; // Nothing found yet, so -1

    for (byte i = 0; i < (sizeof(commandList) / sizeof(command)); i++)
    {
        if (strcmp(inputCommand, commandList[i].name) == false)
        {
            index = i; // Command found @index
            break; // Stop for loop
        }
    }

    if (index != -1 && curArgIter < commandList[index].amountArgs)
        index = -2; // To few arguments are given

    return index;
}

//==============================================================================
// User feedback functions
void terminal::printCommand()
{
    Serial.print(F("$ ")); // Print a $ to represent start of terminal input
    Serial.print(curCommand); // Print current command for convenience
    for (byte i = 0; i < curArgIter; i++)
    {
        Serial.print(F(" ")); // Space between words
        Serial.print(curArgs[i]); // Print all arguments for convenience
    }
    Serial.println(); // End the terminal input line
}

void terminal::printInvalidCommand(int errorCode)
{
    if (errorCode == -1) // Command not found
    {
        Serial.print(curCommand);
        Serial.println(F(": command not found"));
    }
    else if (errorCode == -2) // Not enough arguments
    {
        Serial.print(curCommand);
        Serial.println(F(": command needs more arguments"));
    }
}

//==============================================================================
// Append char to a char* (string)
// Credit: Max van Meijeren
char* terminal::chrcat(char* appendTo, char what)
{
    byte len = strlen(appendTo); // Get string length
    if (len == (MAX_COMMAND_NAME_CHAR - 1)) return appendTo; // String to long
    appendTo[len] = what; // Add char to string
    appendTo[len + 1] = 0; // Add the 0 terminator again
    return appendTo; // Return the new string
}

//==============================================================================
// Execute a terminal command function
void terminal::runCommand(byte index, char** args)
{
    void (*commandFunction)(char** args) = commandList[index].commandFunction;
    commandFunction(args); // Execute a function from the commandList
}

//==============================================================================
// Command functions

void terminal::help(char** args)
{
    Serial.println(F("available commands:"));
    for (int i = 0; i < sizeof(commandList) / sizeof(command); i++)
    {
        Serial.print(F("- "));
        Serial.println(commandList[i].name); // Print every command name
    }
}

void terminal::createFAT(char** args)
{
    storage::initFAT(); // Initialize the File Allocation Table
}

void terminal::store(char** args)
{
    int size = strlen(args[1]) + 1; // Determine filesize + zero termination

    // Try to add the file to storage
    int result = storage::addFile(args[0], size, (byte*) args[1]);
    switch (result) // Print the result
    {
        case (1):
            Serial.print(F("stored: "));
            Serial.println(args[0]);
            break;
        case (-1):
            Serial.println(F("filename to long"));
            break;
        case (-2):
            Serial.println(F("not enough space"));
            break;
        case (-3):
            Serial.println(F("filename exists"));
    }
}

void terminal::retrieve(char** args)
{
    if (!storage::readFile(args[0])) // Try to read and print a file by filename
        Serial.println(F("file not found"));
}

void terminal::erase(char** args)
{
    if (storage::deleteFile(args[0])) // Check if we can delete it
    {
        Serial.print(F("deleted: "));
        Serial.println(args[0]);
    }
    else // Unable to delete file
        Serial.println(F("file not found"));
}

void terminal::files(char** args)
{
    storage::listFiles(); // List all files in FAT
}

void terminal::freespace(char** args)
{
    Serial.print(F("files in FAT: "));
    Serial.print(storage::noOfFiles); // Number of files in FAT
    Serial.print(F("/"));
    Serial.println(FAT_SIZE); // Size of FAT
    Serial.print(F("free space: "));
    Serial.print(storage::freeMemSpace()); // Returns the largest gap in bytes
    Serial.println(F(" bytes"));
}

void terminal::run(char** args)
{
    int result = process::processStart(args[0]); // Try to start a process
    switch (result) // Print the result
    {
        case (1):
            Serial.print(F("started: "));
            Serial.println(args[0]);
            break;
        case (-1):
            Serial.println(F("too many processes"));
            break;
        case(-2):
            Serial.println(F("file does not exist"));
    }
}

void terminal::list(char** args)
{
    process::processList(); // Prints all running or suspended processes
}

void terminal::suspend(char** args)
{
    if (process::setState(atoi(args[0]), 'p')) // Try to set state
        process::printProcess(atoi(args[0]));
    else // State was not changed
        Serial.println(F("could not change state"));
}

void terminal::resume(char** args)
{
    if (process::setState(atoi(args[0]), 'r')) // Try to set state
        process::printProcess(atoi(args[0]));
    else // State was not changed
        Serial.println(F("could not change state"));
}

void terminal::kill(char** args)
{
    if (process::setState(atoi(args[0]), 't')) // Try to set state
        process::printProcess(atoi(args[0]));
    else // State was not changed
        Serial.println(F("could not change state"));
}
