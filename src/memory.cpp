/*
 * Memory Manager
 */

#include "memory.h"
#include "instruction_set.h"

byte memory::noOfVars = 0;
byte memory::memory[MEMORY_SIZE];
memory::variable memory::varTable[TABLE_SIZE];

//==============================================================================
// Set variable from stack into memory table
// Returns -1 if variable couldn't be stored
// Returns 1 if variable is stored in memory
int memory::setVar(byte name, int processID, stack::stack* stack)
{
    if (noOfVars > TABLE_SIZE)
        return -1; // Returns -1 if memory is full

    // Check if we need to overwrite a variable
    int index = getVarIndex(name, processID);
    if (index >= 0 && index < noOfVars)
        deleteVar(index);

    // Start writing variable:
    varTable[noOfVars].name = name;
    varTable[noOfVars].processID = processID;
    varTable[noOfVars].type = stack::popByte(stack);

    // Determine size, pop size from stack if string
    varTable[noOfVars].size = varTable[noOfVars].type;
    if (varTable[noOfVars].type == STRING)
        varTable[noOfVars].size = stack::popByte(stack);

    // Check size available in memory
    varTable[noOfVars].address = memory::getStartPos(varTable[noOfVars].size);
    if (varTable[noOfVars].address == -1)
        return -1; // Not enough space left

    // Write data in reverse order
    for (int i = varTable[noOfVars].size - 1; i >= 0; i--)
        memory[varTable[noOfVars].address + i] = stack::popByte(stack);

    noOfVars++; // Added a variable to memory
    return 1;
}

//==============================================================================
// Get variable from memory table and push it onto stack
// Returns -1 if index is out of bounds
// Returns 1 if variable is pushed onto stack
int memory::getVar(byte name, int processID, stack::stack* stack)
{
    // Get variable index
    int index = getVarIndex(name, processID);
    if (index < 0 || index > noOfVars)
        return -1; // Index is out of bounds

    // Push data on stack
    for (int i = 0; i < varTable[index].size; i++)
        stack::pushByte(stack, memory[varTable[index].address + i]);

    // Push length onto stack if variable is string
    if (varTable[index].type == STRING)
        stack::pushByte(stack, varTable[index].size);

    // Push type onto stack
    stack::pushByte(stack, varTable[index].type);

    return 1; // Pushed variable onto stack
}

//==============================================================================
// Delete all variables of a certain processID
void memory::clearVars(int processID)
{
    for (byte i = 0; i < noOfVars; i++)
    {
        if (varTable[i].processID == processID)
            deleteVar(i--); // Shifts all entries up, so check i-- again
    }
}

void memory::deleteVar(byte index)
{
    // Delete a variable by shifting all other variables after it up
    while (varTable[index+1].name != NULL)
    {
        // Replace the index variable with the variable after it
        varTable[index] = varTable[index+1];
        index++; // Goto the next variable
    }
    varTable[index] = variable{}; // Set last variable to NULL
    noOfVars--; // Decrement the number of variables
}

//==============================================================================
// Get variable index by name and processID
// Return index if a index if found
// Return -1 if no variable is found
int memory::getVarIndex(byte name, int processID)
{
    for (byte i = 0; i < noOfVars; i++)
        if (varTable[i].name == name && varTable[i].processID == processID)
            return i; // Return index of found variable
    return -1; // Did not find variable
}

//==============================================================================
// Get start index by size
// Return 0 if memory is empty
// Return -1 if memory is full
// Return index of first free space for <size>
int memory::getStartPos(int size)
{
    if (noOfVars == 0 && size <= MEMORY_SIZE)
        return 0; // Write to first address

    for (byte i = 0; i < noOfVars-1; i++)
    {
        // Check if there is enough space between variables
        if (varTable[i+1].address - (varTable[i].address + varTable[i].size)
        >= size)
            return varTable[i].address + varTable[i].size;
    }

    // Check space after last variable
    if (MEMORY_SIZE - (varTable[noOfVars-1].address + varTable[noOfVars-1].size)
    >= size)
        return varTable[noOfVars-1].address + varTable[noOfVars-1].size;

    return -1; // No space was found
}
