/*
 * Process Manager
 */

#include "process.h"
#include "storage.h"
#include "memory.h"
#include "instruction.h"
#include "instruction_set.h"

byte process::noOfProcess = 0;
process::process process::proTable[PROCESS_TABLE_SIZE];

//==============================================================================
// Go through the running processes and execute them one by one
void process::runPrograms()
{
    // Check state of all processID's
    int nrOfRunningProcesses = 0; // Check amount of runnnig programs
    for (byte i = 0; i < PROCESS_TABLE_SIZE; i++)
    {
        if (proTable[i].state == 'r') nrOfRunningProcesses++;
        if (proTable[i].state == 't') processKill(i); // Kill process
    }
    if (nrOfRunningProcesses == 0) return; // Nothing is running

    // Setup program time handling
    unsigned long startTime = 0;
    unsigned long runTime = ceil((double)RUN_PROCESS_TIME /
                                (double)nrOfRunningProcesses);

    // Execute running programs while runTime is not exceeded
    // Credit: Thomas Graafland
    for (byte i = 0; i < PROCESS_TABLE_SIZE; i++)
    {
        startTime = millis(); // startTime when program started running
        while (proTable[i].state == 'r' && (millis() - startTime < runTime))
        {
            // Execute a line of the program
            bool terminated = execute(i);
            if (terminated)
            {
                i--;
                break; // Goto next program in for loop
            }
        }
    }
}

//==============================================================================
// Lists all processes
void process::processList()
{
    for (byte i = 0; i < PROCESS_TABLE_SIZE; i++) // Loop through processes
        if (proTable[i].state != '\0') // Check the state of the process
            printProcess(i); // Print process info
}

//==============================================================================
// Prints process details
void process::printProcess(int index)
{
    Serial.print(F("process: "));
    Serial.print(proTable[index].processID); // Process ID
    Serial.print(F(" - "));
    Serial.print(proTable[index].state); // State of the process
    Serial.print(F(" - "));
    Serial.println(proTable[index].name); // Name of the process
}

//==============================================================================
// Starts a process based on the filename
// Returns -1 if to many processes
// Returns -2 if file not found
// Returns 1 if the process started
int process::processStart(char* fileName)
{
    // Check amount of processes
    if (noOfProcess >= PROCESS_TABLE_SIZE)
        return -1; // To many processes

    // Check if file exists
    int index = storage::nameToFATEntry(fileName);
    if (index == -1)
        return -2; // File not found

    // get first free process ID
    int id;
    for (byte i = 0; i < PROCESS_TABLE_SIZE; i++)
    {
        if (proTable[i].state == '\0')
        {
            id = i;
            break;
        }
    }

    // Create process in process table
    process proc;
    strcpy(proc.name, fileName);
    proc.processID = id;
    proc.state = 'r';
    proc.stack.sp = 0;
    proc.filePtr = 0;
    proc.proCnt = storage::getStartPos(index);
    proc.loopAddress = proc.proCnt;

    // Store process in process table
    proTable[id] = proc;
    noOfProcess++;
    return 1;
}

//==============================================================================
// Kills a process based on processID
// Returns false if the process could not be killed
// Returns true if the process was killed
void process::processKill(int index)
{
    memory::clearVars(index); // Delete all variables of process

    // Reset values of process in process table
    strcpy(proTable[index].name, '\0');
    proTable[index].state      = NULL;
    proTable[index].processID  = NULL;
    proTable[index].stack      = {};
    proTable[index].filePtr    = NULL;
    proTable[index].proCnt     = NULL;
    proTable[index].loopAddress = NULL;

    noOfProcess--; // Decrement number of processes
}

//==============================================================================
// Checks if a process exists in the process table
// Returns -1 if the process could not be found
// Returns the processID if a process was found
int process::processExists(int index)
{
    for (byte i = 0; i < PROCESS_TABLE_SIZE; i++)
    {
        // Go through each process in the process table
        if (proTable[i].processID == index && proTable[i].state != NULL)
            return i; // Return the process index
    }
    return -1; // Found nothing
}

//==============================================================================
// Sets a state for a specific processID
// Returns true if the state was changed
// Returns false if the state was not changed
bool process::setState(int index, char state)
{
    // Check if process actually exists
    if (processExists(index) == -1)
        return false;

    // Check the current state
    if (proTable[index].state == state)
        return false;

    proTable[index].state = state; // Set new state
    return true; // State changed
}

//==============================================================================
// Execute the next instruction from process <id> on the EEPROM
// Credit: Thomas Graafland
// Returns true if program is done (terminated)
// Returns false if program is running
bool process::execute(int index)
{
    // byte nextInstruction = EEPROM[process::proTable[id].proCnt++];
    byte nextInstruction = EEPROM[proTable[index].proCnt++];

    // Serial.print(proTable[index].processID, DEC);
    // Serial.print(F(": "));
    // Serial.print(proTable[index].proCnt-1, DEC);
    // Serial.print(F(": "));
    // Serial.println(nextInstruction, DEC);

    switch (nextInstruction)
    {
        // Datatpyes
        case (CHAR):
        case (INT):
        case (STRING):
        case (FLOAT):
            instruction::valueToStack(proTable+index, nextInstruction);
            break;

        // Variables
        case (SET):
            memory::setVar(EEPROM[proTable[index].proCnt++], // Name
                            proTable[index].processID, // ProcessID
                            &(proTable[index].stack)); // Stack pointer
            break;
        case (GET):
            memory::getVar(EEPROM[proTable[index].proCnt++], // Name
                            proTable[index].processID, // ProcessID
                            &(proTable[index].stack)); // Stack pointer
            break;

        // Unary operations
        case (INCREMENT):
        case (DECREMENT):
        case (UNARYMINUS):
        case (ABS):
        case (SQ):
        case (SQRT):
        case (ANALOGREAD):
        case (DIGITALREAD):
        case (LOGICALNOT):
        case (BITWISENOT):
        case (TOCHAR):
        case (TOINT):
        case (TOFLOAT):
        case (ROUND):
        case (FLOOR):
        case (CEIL):
            instruction::unaryOperation(proTable+index, nextInstruction);
            break;

        // Binary operations
        case (PLUS):
        case (MINUS):
        case (TIMES):
        case (DIVIDEDBY):
        case (MODULUS):
        case (EQUALS):
        case (NOTEQUALS):
        case (LESSTHAN):
        case (LESSTHANOREQUALS):
        case (GREATERTHAN):
        case (GREATERTHANOREQUALS):
        case (MIN):
        case (MAX):
        case (POW):
        case (LOGICALAND):
        case (LOGICALOR):
        case (LOGICALXOR):
        case (BITWISEAND):
        case (BITWISEOR):
        case (BITWISEXOR):
            instruction::binaryOperation(proTable+index, nextInstruction);
            break;

        // Arduino operations
        case (CONSTRAIN):
        case (MAP):
        case (PINMODE):
        case (DIGITALWRITE):
        case (ANALOGWRITE):
            instruction::arduinoOperation(proTable+index, nextInstruction);
            break;

        // Time operations
        case (DELAY):
        case (DELAYUNTIL):
        case (MILLIS):
            instruction::timeOperation(proTable+index, nextInstruction);
            break;

        // Time operations
        case(IF):
        case(ELSE):
        case(ENDIF):
        case(WHILE):
        case(ENDWHILE):
        case(LOOP):
        case(ENDLOOP):
            instruction::branchOperation(proTable+index, nextInstruction);
            break;

        // Print
        case (PRINT):
            instruction::print(proTable+index);
            break;
        case (PRINTLN):
            instruction::print(proTable+index, true); // Add newline
            break;

        // Terminating
        case (STOP):
            setState(proTable[index].processID, 't');
            return true; // Terminate process
        default:
            Serial.print(F("unknown process instruction: "));
            Serial.println(nextInstruction, DEC);
            setState(proTable[index].processID, 't');
            return true; // Terminate process
    }

    return false; // Program still running (found no STOP)
}
