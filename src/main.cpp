/*
 * Arduino OS
 *
 * Written by:
 * - Lex van Teeffelen
 *
 * With help from:
 * - Max van Meijeren
 * - Jacco Troost
 * - Thomas Graafland
 * - Meralynn Mohabir
 */

#include <Arduino.h>
#include "terminal.h"
#include "storage.h"
#include "process.h"
#include "test_programs.h"


//==============================================================================
// Add programs to File Allocation Table and EEPROM
// Leaving these in can use a lot of dynamic memory!
void writePrograms()
{
    storage::deleteFile("test"); // Always update the test program
    storage::addFile("test", sizeof(test), test);
    storage::addFile("prog1", sizeof(prog1), prog1);
    storage::addFile("prog2", sizeof(prog2), prog2);
    storage::addFile("prog3", sizeof(prog3), prog3);
    storage::addFile("prog4", sizeof(prog4), prog4);
    storage::addFile("prog5", sizeof(prog5), prog5);
    storage::addFile("prog6", sizeof(prog6), prog6);
}

//==============================================================================
// Setup
void setup()
{
    // A Q&D-hack to prevent setup() from running twice after
    //  uploading sketch on Linux (Arduino IDE)
    // This will safe the EEPROM chip from unnecessary read/writes
    delay(200);

    // Setup serial connection
    Serial.begin(9600); // Use default baudrate
    while (!Serial); // Wait for serial connection

    // Run initializing code
    terminal::init(); // Initialize terminal variables
    // storage::initFAT(); // Reset the File Allocation Table (FAT)
    // writePrograms(); // Write all test programs to EEPROM and FAT

    // Done with setup, ready to receive commands
    Serial.println(F("--- Arduino OS ---"));
}

//==============================================================================
// Main loop
void loop()
{
    /*
     * This is the main loop, this loop needs to run as fast as possible
     *
     * runTerminal() will read a char from serial buffer each time it's called
     *  and will execute a command once it and its arguments are loaded
     *
     * runPrograms() will go through all running processes and gives each
     *  process 50 milliseconds to execute as many operations before returning
     */
    terminal::runTerminal(); // Check for commands
    process::runPrograms(); // Run programs
}
