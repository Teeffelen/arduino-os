# Arduino OS

A small operating system for the Arduino Uno, written as an assignment at the
Rotterdam University of Applied Sciences.

Written by:
- Lex van Teeffelen

With help from:
- Max van Meijeren
- Jacco Troost
- Thomas Graafland
- Meralynn Mohabir


## PlatformIO

This code has been written for use with [PlatformIO](https://platformio.org/),
this can compile Arduino code much smaller due to better C++ compile flags
like `-Os` (optimization for code size, `-O0` being the default).

File structure:

```txt
./include/       - Header and include files (.h)
./lib/           - External libraries (not used)
./src/           - Source code files (.cpp)
./test/          - Test code files (not used)
./platformio.ini - Config file, selects board type and upload port (optional)
```

> **Tip:** Use the Arduino IDE Serial Monitor to interface with Arduino OS CLI


## Features

Working Arduino OS features:

- Reading commands from CLI
- Storing, reading, listing and deleting files from EEPROM (with freespace)
- Storing, retrieving and overwriting variables (CHAR, INT, FLOAT and STRING)
- Starting a (bytecode) file as a process (with suspend, kill and resume)
- Keeping track of status, program counter and variables for each process
- Executing commands from bytecode files/processes
- Run multiple processes at once using Round Robin Scheduling (RRS)

Missing features:

- Reading and writing variables to files from a bytecode program/process
- Forking of processes
- bytecode compiler (still manual labor for now)


## Bytecode Instructions

Working bytecode instructions:

- CHAR
- INT
- STRING
- FLOAT
- SET
- GET
- INCREMENT
- DECREMENT
- PLUS
- MINUS
- TIMES
- DIVIDEDBY
- MODULUS
- UNARYMINUS
- EQUALS
- NOTEQUALS
- LESSTHAN
- LESSTHANOREQUALS
- GREATERTHAN
- GREATERTHANOREQUALS
- LOGICALAND
- LOGICALOR
- LOGICALXOR
- LOGICALNOT
- BITWISEAND
- BITWISEOR
- BITWISEXOR
- BITWISENOT
- TOCHAR
- TOINT
- TOFLOAT
- ROUND
- FLOOR
- CEIL
- MIN
- MAX
- ABS
- CONSTRAIN
- MAP
- POW
- SQ
- SQRT
- DELAY
- DELAYUNTIL
- MILLIS
- PINMODE
- ANALOGREAD
- ANALOGWRITE
- DIGITALREAD
- DIGITALWRITE
- PRINT
- PRINTLN
- IF
- ELSE
- ENDIF
- WHILE
- ENDWHILE
- LOOP
- ENDLOOP
- STOP

Missing bytecode instructions:

- OPEN
- CLOSE
- WRITE
- READINT
- READCHAR
- READFLOAT
- READSTRING
- FORK
- WAITUNTILDONE
